<?php


class Person {
	public $firstName;
	public $middleName;
	public $lastName;

	//constructor
	//A constructor is used during the creation of an object to provide the initial values of each property

	public function __construct($firstName, $middleName, $lastName){
		$this->firstName = $firstName;
		$this->middleName = $middleName;
		$this->lastName = $lastName;
	}

	//methods
	public function printName(){
		return "Your full name is $this->firstName $this->lastName. ";
	}
}


//Developer

class Developer extends Person {

	public function printName(){
		return "Your name is $this->firstName $this->middleName $this->lastName and you are a developer.";
	}
}

//Engineer

class Engineer extends Person {

	public function printName(){
		return "Your are an engineer named $this->firstName $this->middleName $this->lastName.";
	}
}














$person = new Person("Senku", "Mat", "Ishigami");
$developer = new Developer("John", "Finch", "Smith");
$engineer = new Engineer("Harold", "Myers", "Reese");